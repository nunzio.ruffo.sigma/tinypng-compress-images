# tinyPNG-compress-images Python

Note: Make sure you have python and pip installed

- Make an account in https://tinypng.com/developers

- Copy your key in `compressimg.py`

- In the terminal run `pip install --upgrade tinify`

- Copy the images that you want to compress in `img_to_compress/`

- run the script in terminal `python compressimg.py`

- Wait for your images to be compressed... 

Done!... Search for your images in `img_compressed/` 

